package com.grishin.task2.executionmanager;

import com.grishin.task2.context.Context;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks);
}
