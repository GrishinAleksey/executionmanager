package com.grishin.task2.executionmanager.impl;

import com.grishin.task2.context.Context;
import com.grishin.task2.context.impl.ContextImpl;
import com.grishin.task2.executionmanager.ExecutionManager;
import com.grishin.task2.threadpool.impl.FixedThreadPool;

public class ExecutionManagerImpl implements ExecutionManager {

    private final FixedThreadPool fixedThreadPool = new FixedThreadPool(5);

    public ExecutionManagerImpl() {
        new FixedThreadPool(5);
    }

    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        fixedThreadPool.execute(callback, tasks);
        fixedThreadPool.start();
        return context;
    }

    ContextImpl context = new ContextImpl(fixedThreadPool);
}
