package com.grishin.task2.context;

public interface Context {

    /**
     * Возвращает количество тасков, которые на текущий момент успешно выполнились.
     */
    int getCompletedTaskCount();

    /**
     * Возвращает количество тасков, при выполнении которых произошел Exception.
     */
    int getFailedTaskCount();

    /**
     * Отменяет выполнения тасков, которые еще не начали выполняться.
     */
    int getInterruptedTaskCount();

    /**
     * Возвращает количество тасков, которые не были выполены из-за отмены (вызовом предыдущего метода).
     */
    void interrupt();

    /**
     * Вернет true, если все таски были выполнены или отменены, false в противном случае.
     */
    boolean isFinished();

}
