package com.grishin.task2.context.impl;

import com.grishin.task2.context.Context;
import com.grishin.task2.threadpool.ThreadPool;
import com.grishin.task2.threadpool.impl.FixedThreadPool;

public class ContextImpl implements Context {

    private final ThreadPool threadPool;

    public ContextImpl(FixedThreadPool threadPool) {
        this.threadPool = threadPool;
    }

    @Override
    public int getCompletedTaskCount() {
        return threadPool.getCompletedTaskCount();
    }

    @Override
    public int getFailedTaskCount() {
        return threadPool.getFailedTaskCount();
    }

    @Override
    public int getInterruptedTaskCount() {
        return threadPool.getInterruptedTaskCount();
    }

    @Override
    public void interrupt() {
        threadPool.stopThreads();
    }

    @Override
    public boolean isFinished() {
        return threadPool.isFinished();
    }
}
