package com.grishin.task2.threadpool.worker;

import java.util.concurrent.LinkedBlockingQueue;

public class Worker extends Thread {

    public volatile int completedTasks = 0;
    public volatile int failedTasks = 0;
    public volatile int interruptedTask = 0;
    public volatile boolean finished = false;
    private final LinkedBlockingQueue<Runnable> queue;

    public Worker(LinkedBlockingQueue<Runnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                try {
                    if (queue.size() > 0 && !finished) {
                        queue.poll().run();
                        completedTasks++;
                    } else {
                        finished = true;
                        interrupt();
                    }
                    sleep(1);
                } catch (InterruptedException e) {
                    interrupt();
                } catch (IllegalArgumentException e) {
                    failedTasks++;
                    System.out.println(e.getMessage());
                }
            }

        }
    }
}
