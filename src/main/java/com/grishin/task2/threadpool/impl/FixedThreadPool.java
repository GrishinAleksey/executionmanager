package com.grishin.task2.threadpool.impl;


import com.grishin.task2.threadpool.ThreadPool;
import com.grishin.task2.threadpool.worker.Worker;

import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private Runnable finalTask;
    private final LinkedBlockingQueue<Runnable> workersQueue;
    private final Worker[] workers;

    public FixedThreadPool(int poolSize) {
        workers = new Worker[poolSize];
        workersQueue = new LinkedBlockingQueue<>();
        for (int i = 0; i < poolSize; i++) {
            workers[i] = new Worker(workersQueue);
        }
    }

    @Override
    public void start() {
        for (Worker worker : workers) {
            worker.start();
        }
    }

    @Override
    public void execute(Runnable finalTask, Runnable... tasks) {
        synchronized (workersQueue) {
            workersQueue.addAll(Arrays.asList(tasks));
            workersQueue.notify();
            this.finalTask = finalTask;
        }
    }

    @Override
    public int getCompletedTaskCount() {
        int result = 0;
        for (Worker worker : workers)
            result += worker.completedTasks;
        return result;
    }

    @Override
    public int getFailedTaskCount() {
        int result = 0;
        for (Worker worker : workers)
            result += worker.failedTasks;
        return result;
    }

    @Override
    public int getInterruptedTaskCount() {
        int result = 0;
        for (Worker worker : workers)
            result += worker.interruptedTask;
        return result;
    }

    @Override
    public boolean isFinished() {
        boolean result = false;
        for (Worker worker : workers)
            result = worker.finished;
        return result;
    }

    @Override
    public void stopThreads() {
        for (Thread t : workers) {
            t.interrupt();
            try {
                t.join();
            } catch (InterruptedException ignored) {

            }
        }
    }

}
