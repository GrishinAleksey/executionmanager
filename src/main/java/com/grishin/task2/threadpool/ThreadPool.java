package com.grishin.task2.threadpool;

public interface ThreadPool {

    /**
     * Запускает потоки. Потоки бездействуют до тех пор,
     * пока не появится новое задание в очереди (см. execute)
     */
    void start();

    /**
     * Складывает задание в очередь. Освободившийся поток
     * должен выполнить это задание. Каждое задание должно
     * быть выполнено ровно 1 раз
     *
     * @param tasks входной параметр Runnable
     */
    void execute(Runnable callback, Runnable... tasks);

    /**
     * Возвращает количество тасков, которые на текущий момент успешно выполнились.
     */
    int getCompletedTaskCount();

    /**
     * Возвращает количество тасков, при выполнении которых произошел Exception.
     */
    int getFailedTaskCount();

    /**
     * Отменяет выполнения тасков, которые еще не начали выполняться.
     */
    int getInterruptedTaskCount();

    /**
     * Вернет true, если все таски были выполнены или отменены, false в противном случае.
     */
    boolean isFinished();

    /**
     * Останавливает все потоки.
     */
    void stopThreads();

}
