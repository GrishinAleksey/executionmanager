package com.grishin.task2;

import com.grishin.task2.context.Context;
import com.grishin.task2.executionmanager.ExecutionManager;
import com.grishin.task2.executionmanager.impl.ExecutionManagerImpl;

import java.util.ArrayList;
import java.util.List;

public class Test {

    static private final int taskCount = 10;

    public static void main(String[] args) {

        Runnable[] tasks = new Runnable[taskCount];
        for (int i = 0; i < tasks.length; i++) {
            int finalI = i + 1;
            tasks[i] = () -> System.out.println(finalI);
        }
        ExecutionManager executionManager = new ExecutionManagerImpl();
        Context context = executionManager.execute(() -> System.out.println("Конечная задача"), tasks);
        List<String> reportList = new ArrayList<>();
        int tmp = 0;
        while (!context.isFinished()) {
            if (tmp < context.getCompletedTaskCount()) {
                tmp = context.getCompletedTaskCount();
                reportList.add("Задача № " + tmp + " завершена: исключений = " + context.getFailedTaskCount() + "; прервано = " + context.getInterruptedTaskCount());
            }
            Thread.yield();
        }
        reportList.add("Задача № " + tmp + " завершена: исключений = " + context.getFailedTaskCount() + "; прервано = " + context.getInterruptedTaskCount());
        System.out.println();
        for (String line : reportList)
            System.out.println(line);
        if (taskCount == context.getCompletedTaskCount() + context.getFailedTaskCount() + context.getInterruptedTaskCount()) {
            System.out.println();
            System.out.println("Тест прошел успешно: " + context.getCompletedTaskCount() + " успешно выполненных задач, " + context.getFailedTaskCount() + " проваленных задач, " + context.getInterruptedTaskCount() + " остановленных задач");
        } else {
            System.out.println();
            System.out.println("Тест провален");
        }
    }
}
