package com.grishin.task1.exception;

public class TaskException extends RuntimeException {
    public TaskException(Exception message) {
        super(message);
    }
}
