package com.grishin.task1;

import com.grishin.task1.exception.TaskException;

import java.util.concurrent.Callable;

public class Task<T> {

    private final Callable<? extends T> callable;
    private volatile T result;

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public T get() {
        if (result != null) {
            return result;
        }
        if (result == null) {
            synchronized (this) {
                if (result == null) {
                    try {
                        result = callable.call();
                    } catch (Exception e) {
                        throw new TaskException(e);
                    }
                }
            }
        }
        return result;
    }

}
