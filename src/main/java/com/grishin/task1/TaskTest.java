package com.grishin.task1;

public class TaskTest {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            final int finalI = i;
            final int finalI1 = i;
            final Task<Integer> task = new Task<>(() -> finalI * finalI1);
            for (int j = 0; j < 5; j++) {
                new Thread(() -> {
                    try {
                        System.out.println(Thread.currentThread().getName() + ": " + task.get());
                    } catch (Exception exception) {
                        System.out.println(exception.getMessage());
                    }
                }).start();
            }
        }
    }
}
